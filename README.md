This code has moved!
The virtio-fs example daemon is now included in our QEMU tree;
see the updated build instructions for how to built it from there.
